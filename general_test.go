package dbsync

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

var (
	ParkCode   = "1234567890"
	TableName  = "test_table"
	updateTime = time.Now().Unix()
)

// 通用测试流程
func GeneralTest(
	fromDB *gorm.DB,
	toDB *gorm.DB,
	sqlType string,
) (err error) {
	// 初始化表
	if fromDB.HasTable(&TestTable{}) {
		fromDB.DropTable(&TestTable{})
	}
	fromDB.AutoMigrate(&TestTable{})
	if toDB.HasTable(&TestTable{}) {
		toDB.DropTable(&TestTable{})
	}
	toDB.AutoMigrate(&TestTable{})
	var checkItem TestTable
	// 测试新增数据
	item := TestTable{
		ParkCode: ParkCode,
		ID:       1,
		Data:     "data1",
		Info:     "id1",
		Enabled:  true,
	}
	if err = fromDB.Create(&item).Error; err != nil {
		fmt.Println("新增数据-创建item失败")
		return
	}
	if err = TransferBetweenDB(fromDB, toDB, sqlType); err != nil {
		fmt.Println("新增数据-传输item失败")
		return
	}
	if checkItem, err = QueryItem(toDB, 1); err != nil {
		fmt.Println("新增数据-查询item结果失败")
		return
	} else {
		str, _ := json.Marshal(checkItem)
		fmt.Printf("新增数据-结果 %+v\n", string(str))
		if checkItem.Info != item.Info {
			err = errors.New("新增数据-测试失败")
			return
		}
	}
	// 测试更新数据
	err = fromDB.Model(&item).Updates(map[string]interface{}{
		"data": "data2",
	}).Error
	if err != nil {
		return
	}
	if err = TransferBetweenDB(fromDB, toDB, sqlType); err != nil {
		fmt.Println("更新数据-传输item失败")
		return
	}
	if checkItem, err = QueryItem(toDB, 1); err != nil {
		fmt.Println("更新数据-查询item结果失败")
		return
	} else {
		str, _ := json.Marshal(checkItem)
		fmt.Printf("更新数据-结果 %+v\n", string(str))
		if checkItem.Data != item.Data {
			err = errors.New("更新数据-测试结果失败")
			return
		}
		if checkItem.Enabled != item.Enabled {
			err = errors.New("更新数据-测试布尔值失败")
			return
		}
	}
	// 测试删除数据
	if err = fromDB.Delete(&item).Error; err != nil {
		return
	}
	if err = TransferBetweenDB(fromDB, toDB, sqlType); err != nil {
		fmt.Println("删除数据-传输item失败")
		return
	}
	if checkItem, err = QueryItem(toDB.Unscoped(), 1); err != nil {
		fmt.Println("删除数据-查询item结果失败")
		return
	} else {
		str, _ := json.Marshal(checkItem)
		fmt.Printf("删除数据-结果 %+v\n", string(str))
		if checkItem.Info != item.Info {
			err = errors.New("删除数据-测试结果失败")
			return
		}
		if checkItem.DeletedAt == nil {
			err = errors.New("删除数据-测试删除时间失败")
			return
		}
	}
	return
}

// 从库中获取，并插入到另一个表里
func TransferBetweenDB(
	fromDB *gorm.DB,
	toDB *gorm.DB,
	sqlType string,
) (err error) {
	// 从fromDB中获取
	rsp, err := DoFetch(fromDB.DB(), TableName, FetchOptions{
		IgnoreFields:        []string{"park_code"},
		PageNumber:          1,
		PageSize:            100,
		UpdateTimeFieldName: "update_time",
		LastUpdateTime:      updateTime,
		WhereSqlStmt:        "park_code = ?",
		WhereSqlArgs:        []interface{}{ParkCode},
	})
	if err != nil {
		fmt.Println("获取fromDB失败")
		return
	}
	// 模拟传输Json编码和解码
	str, _ := json.Marshal(rsp)
	var req FetchResult
	_ = json.Unmarshal(str, &req)
	// 写入到toDB中
	primaryKeys := make([]string, 0)
	for _, primaryField := range toDB.NewScope(&TestTable{}).GetModelStruct().PrimaryFields {
		primaryKeys = append(primaryKeys, primaryField.DBName)
	}
	err = DoUpdate(toDB.DB(), TableName, req.Data, UpdateMultiOptions{
		UpdateOptions: UpdateOptions{
			Columns:      req.Columns,
			TimeFields:   []string{"create_time", "update_time", "delete_time"},
			FixedFields:  map[string]interface{}{"park_code": ParkCode},
			UniqueFields: primaryKeys,
			SqlType:      sqlType,
		},
		BatchCount: 100,
	})
	if err != nil {
		fmt.Println("插入toDB失败")
		return
	}
	// 更新时间戳
	updateTime = time.Now().Unix()
	return
}

// 表结构
type TestTable struct {
	CreatedAt time.Time  `gorm:"type:datetime;column:create_time"`       // 创建时间
	UpdatedAt time.Time  `gorm:"type:datetime;column:update_time;index"` // 更新时间
	DeletedAt *time.Time `gorm:"type:datetime;column:delete_time;index"` // 删除时间
	ParkCode  string     `gorm:"not null;type:char(10);primary_key"`     // 车场编号
	ID        uint64     `gorm:"auto_increment:false;primary_key"`       // 记录ID
	Data      string     `gorm:"not null;size:100"`                      // 数据信息，可变
	Info      string     `gorm:"not null;size:50"`                       // 标识信息，创建后不变
	Enabled   bool       `gorm:"default:0"`                              // 是否启用
}

func (TestTable) TableName() string {
	return TableName
}

func QueryItem(db *gorm.DB, id uint64) (item TestTable, err error) {
	err = db.First(&item, "park_code = ? and id = ?", ParkCode, id).Error
	return
}
