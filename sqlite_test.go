package dbsync

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"testing"
)

// 测试SQLite
func TestSQLite(t *testing.T) {
	var err error
	defer func() {
		if err != nil {
			t.Error(err)
		}
	}()
	// 来源数据库
	fromDB, err := gorm.Open("sqlite3", "from.db")
	if err != nil {
		return
	}
	defer fromDB.Close()
	// 目标数据库
	toDB, err := gorm.Open("sqlite3", "to.db")
	if err != nil {
		return
	}
	defer toDB.Close()
	// 开启流程测试
	err = GeneralTest(fromDB, toDB, SqlTypeSqlite)
}
